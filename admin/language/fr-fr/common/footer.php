<?php
//////////////////////////////////////
//									//
// Opencart France					//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Text
$_['text_footer']	= 'OpenCart © 2009-' . date('Y') . ' Tous droits réservés.
<br />
Distribution Française intégrale par <a href="http://www.hosteco.fr" target="_blank">hosteco.fr</a> pour <a href="http://opencart-france.eu" target="_blank">Opencart-France.eu</a>
<br />';
$_['text_version']	= 'Version %s';
?>
