<?php
// Text
$_['text_footer']  = 'OpenCart &copy; 2009-' . date('Y') . ' All Rights Reserved.<br />
Full French distribution by <a href="http://www.hosteco.fr" target="_blank">hosteco.fr</a> for Opencart France &trade;';
$_['text_version'] = 'Version %s';
