<?php 
// Copyright (c) 2013-2016 Cesa Agency pour hosteco.fr
// All rights reserved. ---

class ModelExtensionPaymentPayplugh extends Model {
  	public function getMethod($address, $total) {	

			$this->load->language('extension/payment/payplugh');
		
		if (($this->config->get('payment_payplugh_status')) && 
    (sprintf("%0.0f",$this->cart->getTotal() ) >= $this->config->get('payment_payplugh_amount_min')) &&
    (sprintf("%0.0f",$this->cart->getTotal() ) <= $this->config->get('payment_payplugh_amount_max')) ){

     $jsonAnswer = json_decode(html_entity_decode($this->config->get('payment_payplugh_cfg_site')));

     if ((sprintf("%0.0f",$this->cart->getTotal() ) >= $jsonAnswer->amount_min) &&
        (sprintf("%0.0f",$this->cart->getTotal() ) <= $jsonAnswer->amount_max) ){

    	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_payplugh_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
			
			if (!$this->config->get('payment_payplugh_geo_zone_id')) {
        		$status = TRUE;
      		} elseif ($query->num_rows) {
      		  	$status = TRUE;
      		} else {
     	  		$status = FALSE;
			    }	
      	} else { 
			$status = FALSE;
		    }
    } else {
			$status = FALSE;
		}

		
		$method_data = array();
	
		if ($status) {  
      		$method_data = array( 
        		'code'       => 'payplugh',
        		'title'      => $this->language->get('text_title'),
    				'terms'      => '',         		
		    		'sort_order' => $this->config->get('payment_payplugh_sort_order')
      		);
    	}
   
    	return $method_data;
  	}
}
?>
