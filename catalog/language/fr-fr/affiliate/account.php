<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Mon compte affilié';

// Text
$_['text_account']			= 'Compte';
$_['text_my_account']		= 'Mon compte affilié';
$_['text_my_tracking']		= 'Mon suivi d’information';
$_['text_my_transactions']	= 'Mes transactions';
$_['text_edit']				= 'Éditer les informations de Mon compte affilié';
$_['text_password']			= 'Changer mon mot de passe';
$_['text_payment']			= 'Changer mes préférences de paiement';
$_['text_tracking']			= 'Personnaliser le suivi du code affilié';
$_['text_transaction']		= 'Voir l’historique des transactions';
?>