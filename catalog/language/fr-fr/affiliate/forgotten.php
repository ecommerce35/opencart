<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Oubli de votre mot de passe ?';

// Text
$_['text_account']		= 'Compte';
$_['text_forgotten']	= 'Mot de passe oublié';
$_['text_your_email']	= 'Votre adresse courriel';
$_['text_email']		= 'Veuillez entrer l’adresse courriel associée à votre compte, puis cliquez sur valider pour recevoir votre mot de passe par retour courriel';
$_['text_success']		= 'Félicitations, un nouveau mot de passe a été envoyé à votre adresse courriel !';

// Entry
$_['entry_email']		= 'Adresse courriel';

// Error
$_['error_email']		= 'Attention, l’adresse courriel n’a pas été trouvée dans nos dossiers, veuillez essayer à nouveau !';
$_['error_approved']	= 'Attention, votre compte doit être approuvé avant que vous puissiez vous connecter.';
?>