<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']	= 'Se déconnecter du compte';

// Text
$_['text_message']	= '<p>Vous avez été déconnecté de votre compte d’affilié.</p>';
$_['text_account']	= 'Compte';
$_['text_logout']	= 'Se déconnecter';
?>