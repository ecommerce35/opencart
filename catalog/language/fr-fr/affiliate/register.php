<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']				= 'Programme d’affiliation';

// Text
$_['text_account']				= 'Compte';
$_['text_register']				= 'Enregistrement affilié';
$_['text_account_already']		= 'Si vous avez déjà créé un compte, veuillez vous connecter à la <a href="%s">page d’identification</a>.';
$_['text_signup']				= 'Pour créer un compte d’affilié, veuillez remplir le formulaire ci-dessous en vous assurant de bien remplir tous les champs nécessaires:';
$_['text_your_details']			= 'Vos informations personnelles';
$_['text_your_address']			= 'Votre adresse';
$_['text_payment']				= 'Information paiement';
$_['text_your_password']		= 'Votre mot de passe';
$_['text_cheque']				= 'Chèque';
$_['text_paypal']				= 'PayPal';
$_['text_bank']					= 'Virement bancaire';
$_['text_agree']				= 'J’ai lu et approuvé la rubrique <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']			= 'Prénom';
$_['entry_lastname']			= 'Nom';
$_['entry_email']				= 'Courriel';
$_['entry_telephone']			= 'Téléphone';
$_['entry_fax']					= 'Fax';
$_['entry_company']				= 'Société';
$_['entry_website']				= 'Site web';
$_['entry_address_1']			= 'Adresse';
$_['entry_address_2']			= 'Complément d’adresse';
$_['entry_postcode']			= 'Code postal';
$_['entry_city']				= 'Ville';
$_['entry_country']				= 'Pays';
$_['entry_zone']				= 'Département';
$_['entry_tax']					= 'Numéro d’identification fiscale';
$_['entry_payment']				= 'Mode de paiement:';
$_['entry_cheque']				= 'Nom du bénéficiaire des chèques:';
$_['entry_paypal']				= 'Compte courriel PayPal';
$_['entry_bank_name']			= 'Nom de la banque';
$_['entry_bank_branch_number']	= 'Numéro ABA/BSB (Direction générale)';
$_['entry_bank_swift_code']		= 'Code SWIFT';
$_['entry_bank_account_name']	= 'Nom du compte';
$_['entry_bank_account_number']	= 'Numéro de compte';
$_['entry_password']			= 'Mot de passe';
$_['entry_confirm']				= 'Confirmation';

// Error
$_['error_exists']				= 'Attention, l’adresse courriel est déjà enregistrée ! ';
$_['error_firstname']			= 'Le Prénom doit être compris entre 1 et 32 caractères ! ';
$_['error_lastname']			= 'Le Nom doit être compris entre 1 et 32 caractères ! ';
$_['error_email']				= 'L’adresse courriel ne semble pas valide ! ';
$_['error_telephone']			= 'Le Téléphone doit être compris entre 3 et 32 caractères ! ';
$_['error_password']			= 'Le Mot de passe doit être compris entre 4 et 20 caractères ! ';
$_['error_confirm']				= 'Le Mot de passe et la Confirmation ne semble pas correspondre !';
$_['error_address_1']			= 'Le Adresse doit être compris entre 3 et 128 caractères !';
$_['error_city']				= 'La Ville doit être compris entre 2 et 128 caractères !';
$_['error_country']				= 'Veuillez sélectionner un pays !';
$_['error_zone']				= 'Veuillez sélectionner un département !';
$_['error_postcode']			= 'Le Code postal doit être compris entre 2 et 10 caractères !';
$_['error_agree']				= 'Attention, vous devez approuver la rubrique %s !';
?>