<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Vos transactions';

// Column
$_['column_date_added']		= 'Date d’ajout';
$_['column_description']	= 'Description';
$_['column_amount']			= 'Montant (%s)';

// Text
$_['text_account']			= 'Compte';
$_['text_transaction']		= 'Vos transactions';
$_['text_balance']			= 'Votre balance actuelle est de :';
$_['text_empty']			= 'Vous n’avez actuellement aucune transaction !';
?>