<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Suivi d’affiliation';

// Text
$_['text_account']		= 'Compte';
$_['text_description']	= 'Afin d’assurer votre paiement pour les recommandations que vous nous envoyez, nous avons besoin pour suivre ces recommandations que vous placiez un code de suivi dans les URL pointant vers notre boutique. Vous pouvez utiliser les outils ci-dessous pour générer des liens vers %s.';

// Entry
$_['entry_code']		= 'Votre code de suivi';
$_['entry_generator']	= 'Générateur de lien de suivi';
$_['entry_link']		= 'Lien de suivi';

// Help
$_['help_generator']	= 'Tapez le nom d’un produit dont vous souhaitez faire un lien';
?>