<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']				= 'Mode paiement';

// Text
$_['text_account']				= 'Compte';
$_['text_payment']				= 'Paiement';
$_['text_your_payment']			= 'Information paiement';
$_['text_your_password']		= 'Votre mot de passe';
$_['text_cheque']				= 'Chèque';
$_['text_paypal']				= 'PayPal';
$_['text_bank']					= 'Virement bancaire';
$_['text_success']				= 'Félicitations, vous avez mis à jour Votre compte avec succès !';

// Entry
$_['entry_tax']					= 'Numéro d’identification fiscale';
$_['entry_payment']				= 'Mode de paiement';
$_['entry_cheque']				= 'Nom du bénéficiaire des chèques:';
$_['entry_paypal']				= 'Compte courriel PayPal';
$_['entry_bank_name']			= 'Nom de la banque';
$_['entry_bank_branch_number']	= 'Numéro ABA/BSB (Direction générale)';
$_['entry_bank_swift_code']		= 'Code SWIFT';
$_['entry_bank_account_name']	= 'Nom du compte';
$_['entry_bank_account_number']	= 'Numéro de compte';
?>