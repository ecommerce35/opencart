<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Mon compte information';

// Text
$_['text_account']		= 'Compte';
$_['text_edit']			= 'Éditer Mon compte information';
$_['text_your_details']	= 'Vos détails personnels';
$_['text_your_address']	= 'Votre adresse';
$_['text_success']		= 'Félicitations, vous avez modifié Mon compte information avec succès !';

// Entry
$_['entry_firstname']	= 'Prénom';
$_['entry_lastname']	= 'Nom';
$_['entry_email']		= 'Courriel';
$_['entry_telephone']	= 'Téléphone';
$_['entry_fax']			= 'Fax';
$_['entry_company']		= 'Société';
$_['entry_website']		= 'Site web';
$_['entry_address_1']	= 'Adresse';
$_['entry_address_2']	= 'Complément d’adresse';
$_['entry_postcode']	= 'Code postal';
$_['entry_city']		= 'Ville';
$_['entry_country']		= 'Pays';
$_['entry_zone']		= 'Département';

// Error
$_['error_exists']		= 'Attention, cette adresse courriel est déjà enregistrée !';
$_['error_firstname']	= 'Le Prénom doit être compris entre 1 et 32 caractères !';
$_['error_lastname']	= 'Le Nom doit être compris entre 1 et 32 caractères !';
$_['error_email']		= 'L’Adresse courriel ne semble pas valide !';
$_['error_telephone']	= 'Le Téléphone doit être compris entre 3 et 32 caractères !';
$_['error_address_1']	= 'L’Adresse 1 doit être compris entre 3 et 128 caractères !';
$_['error_city']		= 'La Ville doit être compris entre 2 et 128 caractères !';
$_['error_country']		= 'Veuiller sélectionner un pays !';
$_['error_zone']		= 'Veuiller sélectionner un département !';
$_['error_postcode']	= 'Le Code postal doit être compris entre 2 et 10 caractères pour ce pays !';
?>