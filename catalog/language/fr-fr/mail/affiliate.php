<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Text
$_['text_subject']				= '%s - Programme d’affiliation';
$_['text_welcome']				= 'Nous vous remercions de votre enregistrement au programme d’affiliation et vous souhaitons la bienvenue sur %s !';
$_['text_login']				= 'Votre compte a été créé, vous pouvez désormais vous connecter en utilisant votre adresse courriel et mot de passe è l’adresse suivante :';
$_['text_approval']				= 'Votre compte a été approuvé, vous pouvez désormais vous connecter en utilisant votre adresse courriel et mot de passe à l’adresse suivante :';
$_['text_services']				= 'Après votre connexion, vous serez en mesure de générer des codes de suivi, de suivre le paiement de vos commissions et de modifier vos informations de compte.';
$_['text_thanks']				= 'Remerciements,';
$_['text_new_affiliate']		= 'Nouvel affilié';
$_['text_signup']				= 'Une nouvel affilié vient de signer :';
$_['text_store']				= 'Boutique :';
$_['text_firstname']			= 'Prénom :';
$_['text_lastname']				= 'Nom :';
$_['text_company']				= 'Société :';
$_['text_email']				= 'Courriel :';
$_['text_telephone']			= 'Téléphone :';
$_['text_website']				= 'Site web :';
$_['text_order_id']				= 'Identifiant commande :';
$_['text_transaction_subject']	= '%s - Commission d’affiliation';
$_['text_transaction_received']	= 'Vous avez reçu %s de commission !';
$_['text_transaction_total']	= 'Le montant total de vos commisions est désormais de %s.';
?>