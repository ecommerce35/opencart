<?php
// Text
$_['text_subject']        = 'Nous vous remercions pour votre inscription sur %s';
$_['text_welcome']        = 'Bienvenue et merci de votre inscription sur %s!';
$_['text_login']          = 'Votre compte a été créé et vous pouvez vous connecter en utilisant votre adresse e-mail et votre mot de passe en visitant notre site Web ou à l\'URL suivante:';
$_['text_approval']       = 'Votre compte doit être approuvé avant de pouvoir vous connecter. Une fois approuvé, vous pouvez vous connecter en utilisant votre adresse email et votre mot de passe en visitant notre site web ou à l\'URL suivante:';
$_['text_service']        = 'Une fois connecté, vous pourrez accéder à d\'autres services, notamment consulter les commandes passées, imprimer des factures et modifier les informations de votre compte.';
$_['text_thanks']         = 'Cordialement,';
$_['text_new_customer']   = 'Inscription d\'un nouveau client';
$_['text_signup']         = 'Un nouveau client s\'est inscrit:';
$_['text_customer_group'] = 'Groupe client:';
$_['text_firstname']      = 'Prénom:';
$_['text_lastname']       = 'Nom:';
$_['text_email']          = 'E-Mail:';
$_['text_telephone']      = 'Téléphone:';
