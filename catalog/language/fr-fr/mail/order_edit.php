<?php
// Text
$_['text_subject']      = '%s - Mise à jour Commande n° %s';
$_['text_order_id']     = 'N° de Commande :';
$_['text_date_added']   = 'Date de création :';
$_['text_order_status'] = 'Votre commande a été mise à jour avec le statut suivant :';
$_['text_comment']      = 'Les commentaires pour votre commande sont :';
$_['text_link']         = 'Pour voir votre commande, cliquez sur le lien ci-dessous :';
$_['text_footer']       = 'S\'il vous plaît répondre à cet e-mail si vous avez des questions.';
