<?php
// Text
$_['text_subject']      = '%s - Commande n° %s';
$_['text_received']     = 'Vous avez reçu une commande.';
$_['text_order_id']     = 'N° de Commande :';
$_['text_date_added']   = 'Date de création :';
$_['text_order_status'] = 'État de la commande :';
$_['text_product']      = 'Produits';
$_['text_total']        = 'Totaux';
$_['text_comment']      = 'Les commentaires pour votre commande sont:';
