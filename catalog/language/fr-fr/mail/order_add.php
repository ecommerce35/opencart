<?php
// Text
$_['text_subject']          = '%s - Votre Commande n° %s';
$_['text_greeting']         = 'Merci de votre intérêt pour %s produits. Votre commande a été reçue et sera traitée une fois le paiement confirmé..';
$_['text_link']             = 'Pour voir votre commande, cliquez sur le lien ci-dessous:';
$_['text_order_detail']     = 'Détails de la Commande';
$_['text_instruction']      = 'Instructions';
$_['text_order_id']         = 'N° de Commande :';
$_['text_date_added']       = 'Date de création :';
$_['text_order_status']     = 'État de la commande :';
$_['text_payment_method']   = 'Mode de paiement :';
$_['text_shipping_method']  = 'Mode de livraison :';
$_['text_email']            = 'E-mail :';
$_['text_telephone']        = 'Téléphone:';
$_['text_ip']               = 'Address IP :';
$_['text_payment_address']  = 'Adresse de Facturation';
$_['text_shipping_address'] = 'Adresse de livraison';
$_['text_products']         = 'Produits';
$_['text_product']          = 'Produit';
$_['text_model']            = 'Modèle';
$_['text_quantity']         = 'Quantité';
$_['text_price']            = 'Prix';
$_['text_order_total']      = 'Totaux de la commande';
$_['text_total']            = 'Total';
$_['text_download']         = 'Une fois votre paiement validé, vous pouvez cliquer sur le lien ci-dessous pour accéder à vos produits téléchargeables :';
$_['text_comment']          = 'Les commentaires pour votre commande sont :';
$_['text_footer']           = 'S\'il vous plaît répondre à cet e-mail si vous avez des questions.';
