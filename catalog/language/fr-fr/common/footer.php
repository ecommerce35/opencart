<?php
//////////////////////////////////////
//									//
// Opencart France					//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Text
$_['text_information']	= 'Informations';
$_['text_service']		= 'Service client';
$_['text_extra']		= 'Extras';
$_['text_contact']		= 'Nous contacter';
$_['text_return']		= 'Retour de marchandise';
$_['text_sitemap']		= 'Plan du site';
$_['text_manufacturer']	= 'Fabricants';
$_['text_voucher']		= 'Chèques-cadeaux';
$_['text_affiliate']	= 'Affiliations';
$_['text_special']		= 'Promotions';
$_['text_account']		= 'Mon compte';
$_['text_order']		= 'Historique de commandes';
$_['text_wishlist']		= 'Liste de souhaits';
$_['text_newsletter']	= 'Lettre d’information';
$_['text_powered']		= 'Développé Par <b>OpenCart</b> - Propulsé, traduit par <a href="http://www.opencart-france.eu" target="_blank"><b>OpenCart France &trade;</b></a> et <a href="http://www.hosteco.fr" target="_blank"><b>hosteco.fr</b></a> - %s &copy; %s';
?>
