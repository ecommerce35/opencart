<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Text
$_['text_items']		= '%s article(s) - %s';
$_['text_empty']		= 'Votre panier est vide !';
$_['text_cart']			= 'Voir panier';
$_['text_checkout']		= 'Commander';
$_['text_recurring']	= 'Profil de paiement récurrent';
?>