<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Text
$_['text_success']       = 'Vous avez modifié les clients avec succès';

// Error
$_['error_permission']   = 'Attention, vous n’avez pas la permission d’accès à l’API !';
$_['error_firstname']    = 'Le Nom doit être compris entre 1 et 32 caractères !';
$_['error_lastname']     = 'Le Prénom doit être compris entre 1 et 32 caractères !';
$_['error_email']        = 'L’adresse courriel semble invalide !';
$_['error_telephone']    = 'Le numéro de téléphone doit être compris entre 1 et 32 caractères !';
$_['error_custom_field'] = '%s requis !';
?>