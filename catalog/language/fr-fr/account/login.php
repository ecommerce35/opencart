<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']					= 'Se connecter au compte';

// Text
$_['text_account']					= 'Client';
$_['text_login']					= 'Se connecter';
$_['text_new_customer']				= 'Nouveau client';
$_['text_register']					= 'Inscription';
$_['text_register_account']			= 'En créant un compte vous allez pouvoir commander plus facilement et plus vite, être au courant de l’état de vos commandes, et garder une trace de vos commandes précédentes.';
$_['text_returning_customer']		= 'Identification';
$_['text_i_am_returning_customer']	= 'Déjà client';
$_['text_forgotten']				= 'Mot de passe oublié';

// Entry
$_['entry_email']					= 'Adresse courriel';
$_['entry_password']				= 'Mot de passe';

// Error
$_['error_login']					= 'Attention, pas de correspondance entre l’adresse courriel et le mot de passe !';
$_['error_attempts']				= 'Attention, le nombre d’essais de connexion à votre compte est atteint. Merci de bien vouloir recommencer dans 1 houre.';
$_['error_approved']				= 'Attention, votre compte requiert une approbation afin de vous connecter.';
?>