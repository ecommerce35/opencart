<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Vos retours produits';

// Text
$_['text_account']			= 'Compte';
$_['text_return']			= 'Information retour';
$_['text_return_detail']	= 'Détails du retour';
$_['text_description']		= '<p>Veuillez remplir ce formulaire pour en vue d’obtenir un numéro de Bon de retour.</p>';
$_['text_order']			= 'Information commande';
$_['text_product']			= 'Information produit et raison du retour';
$_['text_reason']			= 'Raison du retour';
$_['text_message']			= '<p>Nous vous remercions de l’envoi du formulaire de retour de marchandise. celui-ci a été transmis au service compétent en vue de son traitement.</p><p> Vous serez avisé par courriel de l’état de votre demande.</p>';
$_['text_return_id']		= 'Identifiant retour :';
$_['text_order_id']			= 'Identifiant commande :';
$_['text_date_ordered']		= 'Date de commande :';
$_['text_status']			= 'État :';
$_['text_date_added']		= 'Date d’ajout :';
$_['text_comment']			= 'Commentaires des retours';
$_['text_history']			= 'Historique des retours';
$_['text_empty']			= 'Vous n’avez aucune demande actuelle de retour de produit';
$_['text_agree']			= 'J’ai lu et approuvé la rubrique <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']		= 'Identifiant du retour';
$_['column_order_id']		= 'Identifiant de commande';
$_['column_status']			= 'État';
$_['column_date_added']		= 'Date d’ajout';
$_['column_customer']		= 'Client';
$_['column_product']		= 'Nom du produit';
$_['column_model']			= 'Modèle';
$_['column_quantity']		= 'Quantité';
$_['column_price']			= 'Prix';
$_['column_opened']			= 'Ouverture';
$_['column_comment']		= 'Commentaires';
$_['column_reason']			= 'Raison';
$_['column_action']			= 'Action';

// Entry
$_['entry_order_id']		= 'Identifiant de commande';
$_['entry_date_ordered']	= 'Date de commande';
$_['entry_firstname']		= 'Prénom';
$_['entry_lastname']		= 'Nom';
$_['entry_email']			= 'Courriel';
$_['entry_telephone']		= 'Téléphone';
$_['entry_product']			= 'Nom du produit';
$_['entry_model']			= 'Code produit';
$_['entry_quantity']		= 'Quantité';
$_['entry_reason']			= 'Raison du retour';
$_['entry_opened']			= 'Le dossier du retour produit est ouvert';
$_['entry_fault_detail']	= 'Défectueux ou autres détails';

// Error
$_['text_error']			= 'La demande de retour que vous désirez n’a pas été trouvée !';
$_['error_order_id']		= 'L’identifiant commande est requis !';
$_['error_firstname']		= 'Le Prénom doit être compris entre 1 et 32 caractères ! ';
$_['error_lastname']		= 'Le Nom doit être compris entre 1 et 32 caractères ! ';
$_['error_email']			= 'L’adresse courriel ne semble pas valide ! ';
$_['error_telephone']		= 'Le Téléphone doit être compris entre 3 et 32 caractères ! ';
$_['error_product']			= 'Le Nom du produit doit être compris entre 3 et 255 caractères ! ';
$_['error_model']			= 'Le Modèle du produit doit être compris entre 3 et 64 caractères ! ';
$_['error_reason']			= 'Vous devez sélectionner une raison pour le retour de ce produit !';
$_['error_agree']			= 'Attention, vous devez approuver la rubrique %s !';
?>