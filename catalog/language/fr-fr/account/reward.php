<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Vos points de fidélité';

// Column
$_['column_date_added']		= 'Date d’ajout';
$_['column_description']	= 'Description';
$_['column_points']			= 'Points';

// Text
$_['text_account']			= 'Compte';
$_['text_reward']			= 'Points de fidélité';
$_['text_total']			= 'Le nombre total de vos points de fidélité est de :';
$_['text_empty']			= 'Aucun point de fidélité.';
?>