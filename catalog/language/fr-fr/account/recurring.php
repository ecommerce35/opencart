<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']							= 'Paiements récurrents';

// Text
$_['text_account']							= 'Compte';
$_['text_recurring']						= 'Paiement récurrent';
$_['text_recurring_detail']					= 'Détails du paiement récurrent';
$_['text_order_recurring_id']				= 'Identifiant de la commande récurrente :';
$_['text_date_added']						= 'Créé :';
$_['text_status']							= 'État : ';
$_['text_payment_method']					= 'Mode de paiement :';
$_['text_order_id']							= 'Identifiant ommande :';
$_['text_product']							= 'Produit :';
$_['text_quantity']							= 'Quantité :';
$_['text_description']						= 'Description :';
$_['text_reference']						= 'Référence :';
$_['text_transaction']						= 'Transactions';
$_['text_status_1']							= 'Actif';
$_['text_status_2']							= 'Inactif';
$_['text_status_3']							= 'Annulé';
$_['text_status_4']							= 'Suspendu';
$_['text_status_5']							= 'Expiré';
$_['text_status_6']							= 'En attente';
$_['text_transaction_date_added']			= 'Créé';
$_['text_transaction_payment']				= 'Paiement';
$_['text_transaction_outstanding_payment']	= 'Impayé';
$_['text_transaction_skipped']				= 'Paiement ignoré';
$_['text_transaction_failed']				= 'Paiement échoué';
$_['text_transaction_cancelled']			= 'Annulé';
$_['text_transaction_suspended']			= 'Suspendu';
$_['text_transaction_suspended_failed']		= 'Suspendu pour paiement échoué';
$_['text_transaction_outstanding_failed']	= 'Impayé échoué';
$_['text_transaction_expired']				= 'Expiré';
$_['text_empty']							= 'Aucun paiement récurrent trouvé';
$_['text_error']							= 'La commande récurrente que vous avez demandé n’a pas été trouvée !';
$_['text_cancelled']						= 'Le paiement récurrent a été annulé';

// Column
$_['column_date_added']						= 'Créé';
$_['column_type']							= 'Type';
$_['column_amount']							= 'Montant';
$_['column_status']							= 'État';
$_['column_product']						= 'Produit';
$_['column_order_recurring_id']				= 'Identifiant de la commande récurrente';

// Error
$_['error_not_cancelled']					= 'Erreur : %s';
$_['error_not_found']						= 'Impossible d’annuler le profil';

// Button
$_['button_return']							= 'Retour';
?>