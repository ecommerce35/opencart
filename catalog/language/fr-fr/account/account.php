<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Votre compte';

// Text
$_['text_account']			= 'Compte';
$_['text_my_account']		= 'Votre compte personnel';
$_['text_my_orders']		= 'Vos commandes';
$_['text_my_newsletter']	= 'Votre lettre d’information';
$_['text_edit']				= 'Éditer les informations de votre compte';
$_['text_password']			= 'Changer votre mot de passe';
$_['text_address']			= 'Modifier les entrées de votre carnet d’adresses';
$_['text_credit_card']		= 'Gérer les cartes de crédit enregistrées';
$_['text_wishlist']			= 'Modifier votre liste de souhaits';
$_['text_order']			= 'Votre historique de commande';
$_['text_download']			= 'Vos téléchargements';
$_['text_reward']			= 'Vos points de fidélité'; 
$_['text_return']			= 'Vos retours de marchandises'; 
$_['text_transaction']		= 'Vos transactions'; 
$_['text_newsletter']		= 'S’abonner ou se désabonner à la lettre d’information';
$_['text_recurring']		= 'Paiements récurrents';
$_['text_transactions']		= 'Transactions';
?>