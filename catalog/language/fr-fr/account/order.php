<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.eu	//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']			= 'Votre historique de commande';

// Text
$_['text_account']			= 'Compte';
$_['text_order']			= 'Information commande';
$_['text_order_detail']		= 'Détail de la commande';
$_['text_invoice_no']		= 'Facture N° :';
$_['text_order_id_']			= 'N° de commande : ';
$_['text_date_added']		= 'Date de commande : ';
$_['text_shipping_address']	= 'Adresse de livraison';
$_['text_shipping_method']	= 'Mode de livraison :';
$_['text_payment_address']	= 'Adresse de paiement';
$_['text_payment_method']	= 'Mode de paiement :';
$_['text_comment']			= 'Commentaires commande';
$_['text_history']			= 'Historique de commande';
$_['text_success']			= 'Félicitations, vous avez ajouté avec succès les produits de la commande N° %s à votre panier !';
$_['text_empty']			= 'Vous n’avez aucune commande précédente';
$_['text_error']			= 'La commande que vous désirez n’a pas été trouvée !';

// Column
$_['column_order_id']		= 'N° de commande';
$_['column_customer']		= 'Client';
$_['column_product']		= 'Nombre de produits';
$_['column_name']		  	= 'Nom du produit';
$_['column_model']			= 'Modèle';
$_['column_quantity']		= 'Quantité';
$_['column_price']			= 'Prix';
$_['column_total']			= 'Total';
$_['column_action']			= 'Action';
$_['column_date_added']	= 'Date';
$_['column_status']			= 'État';
$_['column_comment']		= 'Commentaires';

// Error
$_['error_reorder']			= '%s n’est pas disponible actuellement pour être commandé à nouveau';
?>
