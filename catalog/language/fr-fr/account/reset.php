<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// header
$_['heading_title']		= 'Réinitialiser votre mot de passe';

// Text
$_['text_account']		= 'Compte';
$_['text_password']		= 'Entrer le nouveau mot de passe que vous désirez utiliser.';
$_['text_success']		= 'Félicitations, vous avez modifié votre mot de passe avec succès !';

// Entry
$_['entry_password']	= 'Mot de passe';
$_['entry_confirm']		= 'Confirmation';

// Error
$_['error_password']	= 'Le Mot de passe doit être compris entre 4 et 20 caractères !';
$_['error_confirm']		= 'Le Mot de passe et la Confirmation ne semble pas correspondre !';
$_['error_code']		= 'Le Mot de passe de réinitialisation est invalide ou a été utilisé auparavant !';
?>