<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']					= 'Carnet d’adresses';

// Text
$_['text_account']					= 'Compte';
$_['text_address_book']				= 'Entrées du carnet d’adresses';
$_['text_edit_address']				= 'Modifier l’adresse';
$_['text_add']						= 'Votre adresse a été insérée avec succès';
$_['text_edit']						= 'Votre adresse a été mise à jour avec succès';
$_['text_delete']					= 'Votre adresse a été supprimée avec succès';
$_['text_empty']        			= 'Aucune adresse ne se trouve dans votre compte.';

// Entry
$_['entry_firstname']				= 'Prénom';
$_['entry_lastname']				= 'Nom';
$_['entry_company']					= 'Société';
$_['entry_address_1']				= 'Adresse';
$_['entry_address_2']				= 'Complément d’adresse';
$_['entry_postcode']				= 'Code Postal';
$_['entry_city']					= 'Ville';
$_['entry_country']					= 'Pays';
$_['entry_zone']					= 'Département';
$_['entry_default']					= 'Adresse par défaut';

// Error
$_['error_delete']					= 'Attention, vous devez avoir au moins une adresse ! ';
$_['error_default']					= 'Attention, vous ne pouvez pas supprimer votre adresse par défaut ! ';
$_['error_firstname']				= 'Le Prénom doit être compris entre 1 et 32 caractères ! ';
$_['error_lastname']				= 'Le Nom doit être compris entre 1 et 32 caractères ! ';
$_['error_vat']         			= 'Identifiant TVA invalide !';
$_['error_address_1']				= 'L’Adresse doit être comprise entre 3 et 128 caractères !';
$_['error_postcode']				= 'Le Code postal doit être compris entre 2 et 10 caractères !';
$_['error_city']					= 'La Ville doit être comprise entre 2 et 128 caractères !';
$_['error_country']					= 'Veuillez sélectionner un pays !';
$_['error_zone']					= 'Veuillez sélectionner un département !';
$_['error_custom_field']			= '%s requis !';
?>