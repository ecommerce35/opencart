<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Vos téléchargements';

// Text
$_['text_account']		= 'Compte';
$_['text_downloads']	= 'Téléchargements';
$_['text_empty']		= 'Vous n’avez effectué aucune commande téléchargeable ! ';

// Column
$_['column_order_id']	= 'Identifiant commande';
$_['column_name']		= 'Nom';
$_['column_size']		= 'Taille';
$_['column_date_added']	= 'Date d’ajout';
?>