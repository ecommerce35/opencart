<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Vous avez oublié votre mot de passe ? ';

// Text
$_['text_account']		= 'Compte';
$_['text_forgotten']	= 'Mot de passe oublié';
$_['text_your_email']	= 'Votre adresse courriel';
$_['text_email']		= 'Entrez l’adresse courriel associée à votre compte puis cliquez sur « Continuer » afin que votre mot de passe vous soit envoyé.';
$_['text_success']		= 'Félicitations, un nouveau mot de passe a été envoyé à votre adresse courriel.';

// Entry
$_['entry_email']		= 'Adresse courriel';
$_['entry_password']	= 'Nouveau mot de passe';
$_['entry_confirm']		= 'Confirmation';

// Error
$_['error_email']		= 'Attention, l’adresse courriel n’a pas été trouvée dans nos archives, essayez à nouveau ! ';
$_['error_approved']	= 'Attention, votre compte requiert une approbation afin de vous connecter.';
$_['error_password']	= 'Le mot de passe doit être compris entre 4 et 20 caractères ! ';
$_['error_confirm']		= 'Le mot de passe et la confirmation ne semble pas correspondre !';
?>