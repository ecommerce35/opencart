<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']	= 'Votre liste de souhaits';

// Text
$_['text_account']	= 'Compte';
$_['text_instock']	= 'En stock';
$_['text_wishlist']	= 'Liste de souhaits (%s)';
$_['text_login']	= 'Vous pouvez <a href="%s">vous connecter</a> ou <a href="%s">créer un compte</a> pour sauvegarder <a href="%s">%s</a> dans votre <a href="%s"> liste de souhaits</a>&nbsp;!';
$_['text_success']	= 'Félicitations, vous avez ajouté <a href="%s">%s</a> à votre <a href="%s">liste de souhaits</a>&nbsp;!';
$_['text_remove']	= 'Félicitations, vous avez modifié votre liste de souhaits avec succès !';
$_['text_empty']	= 'Votre liste de souhaits est vide.';

// Column
$_['column_image']	= 'Image';
$_['column_name']	= 'Nom du produit';
$_['column_model']	= 'Modèle';
$_['column_stock']	= 'Stock';
$_['column_price']	= 'Prix unitaire';
$_['column_action']	= 'Action';
?>