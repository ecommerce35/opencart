<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']	= 'Votre compte a été créé ! ';

// Text
$_['text_message']	= '<p>Félicitations, votre compte a été créé avec succès !</p><p>Vous pouvez désormais profiter de vos privilèges de membre pour améliorer votre expérience sur notre site.</p><p>Si vous avez des questions sur le fonctionnement de notre boutique en ligne, vous pouvez nous contacter par courriel.</p><p>Une confirmation a été envoyé à l’adresse courriel fournie. Si vous ne l’avez pas reçu dans l’heure, veuillez <a href="%s">nous contacter</a>.</p>';
$_['text_approval']	= '<p>Merci de votre enregistrement chez %s !</p><p>Vous serez informé par courriel dès que votre compte sera activé.</p><p>Si vous avez des questions sur le fonctionnement de notre boutique en ligne, n’hésitez pas à <a href="%s">nous contacter</a>.</p>';
$_['text_account']	= 'Compte';
$_['text_success']	= 'Félicitations !';
?>