<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']					= 'Vos informations de compte';

// Text
$_['text_account']					= 'Compte';
$_['text_edit']						= 'Éditer Vos informations de compte';
$_['text_your_details']				= 'Vos informations personnelles';
$_['text_success']					= 'Félicitations, vos informations de compte ont été mises à jour avec succès.';

// Entry
$_['entry_firstname']				= 'Prénom';
$_['entry_lastname']				= 'Nom';
$_['entry_email']					= 'Courriel';
$_['entry_telephone']				= 'Téléphone';
$_['entry_fax']						= 'Fax';

// Error
$_['error_exists']					= 'Attention, l’adresse courriel est déjà enregistrée ! ';
$_['error_firstname']				= 'Le Prénom doit être compris entre 1 et 32 caractères ! ';
$_['error_lastname']				= 'Le Nom doit être compris entre 1 et 32 caractères ! ';
$_['error_email']					= 'L’adresse courriel ne semble pas valide ! ';
$_['error_telephone']				= 'Le Téléphone doit être compris entre 3 et 32 caractères ! ';
$_['error_custom_field']			= '%s requis !';
?>