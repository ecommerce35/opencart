<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']	= 'Se déconnecter du compte';

// Text
$_['text_message']	= '<p>Vous avez été déconnecté de votre compte. Désormais vous pouvez laisser votre ordinateur.</p><p>Votre panier a été sauvegardé ainsi que les produits qui s’y trouvaient. Ils seront restaurés la prochaine fois que vous vous connecterez à votre compte.</p>';
$_['text_account']	= 'Compte';
$_['text_logout']	= 'Se déconnecter';
?>