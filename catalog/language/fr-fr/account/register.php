<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']					= 'Créer un compte';

// Text
$_['text_account']					= 'Compte';
$_['text_register']					= 'S’enregistrer';
$_['text_account_already']			= 'Si vous avez déjà créé un compte, veuillez vous connecter à la <a href="%s">page d’identification</a>.';
$_['text_your_details']				= 'Vos informations personnelles';
$_['text_your_address']				= 'Votre adresse';
$_['text_newsletter']				= 'Lettre d’information';
$_['text_your_password']			= 'Votre mot de passe';
$_['text_agree']					= 'J’ai lu et approuvé la rubrique <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group']			= 'Groupe clients';
$_['entry_firstname']				= 'Prénom';
$_['entry_lastname']				= 'Nom';
$_['entry_email']					= 'Courriel';
$_['entry_telephone']				= 'Téléphone';
$_['entry_fax']						= 'Fax';
$_['entry_company']					= 'Société';
$_['entry_address_1']				= 'Adresse';
$_['entry_address_2']				= 'Complément d’adresse';
$_['entry_postcode']				= 'Code postal';
$_['entry_city']					= 'Ville';
$_['entry_country']					= 'Pays';
$_['entry_zone']					= 'Département';
$_['entry_newsletter']				= 'Abonnement';
$_['entry_password']				= 'Mot de passe';
$_['entry_confirm']					= 'Confirmation';

// Error
$_['error_exists']					= 'Attention, l’adresse courriel est déjà enregistrée ! ';
$_['error_firstname']				= 'Le Prénom doit être compris entre 1 et 32 caractères ! ';
$_['error_lastname']				= 'Le Nom doit être compris entre 1 et 32 caractères ! ';
$_['error_email']					= 'L’adresse courriel ne semble pas valide ! ';
$_['error_telephone']				= 'Le Téléphone doit être compris entre 3 et 32 caractères ! ';
$_['error_address_1']				= 'L’Adresse 1 doit être compris entre 3 et 128 caractères ! ';
$_['error_city']					= 'La Ville 1 doit être compris entre 2 et 128 caractères ! ';
$_['error_postcode']				= 'Le Code postal doit être compris entre 2 et 10 caractères ! ';
$_['error_country']					= 'Veuillez sélectionner un pays !';
$_['error_zone']					= 'Veuillez sélectionner un département !';
$_['error_custom_field']			= '%s requis !';
$_['error_password']				= 'Le Mot de passe doit être compris entre 4 et 20 caractères !';
$_['error_confirm']					= 'Le Mot de passe et la Confirmation ne semble pas correspondre !';
$_['error_agree']					= 'Attention, vous devez approuver la rubrique %s !';
?>