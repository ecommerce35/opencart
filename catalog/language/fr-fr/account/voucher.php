<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Achat de chèques-cadeaux';

// Text
$_['text_account']		= 'Compte';
$_['text_voucher']		= 'Chèque-cadeau';
$_['text_description']	= 'Ce chèque-cadeau sera envoyé au destinataire après le réglement de votre commande. ';
$_['text_agree']		= 'Je comprends et accepte que les chèques-cadeaux ne puissent pas être remboursables';
$_['text_message']		= '<p>Nous vous remercions pour cet achat de chèque-cadeau ! Une fois votre commande terminée le destinataire sera avisé par courriel des modalités d’èchange de leurs chèques-cadeaux.</p>';
$_['text_for']			= '%s Chèque-cadeau pour %s';

// Entry
$_['entry_to_name']		= 'Nom du destinataire';
$_['entry_to_email']	= 'Courriel du destinataire';
$_['entry_from_name']	= 'Votre nom';
$_['entry_from_email']	= 'Votre courriel';
$_['entry_theme']		= 'Thème';
$_['entry_message']		= 'Message';
$_['entry_amount']		= 'Montant';

// Help
$_['help_message']     = '(Optionnel)';
$_['help_amount']      = '(La valeur doit être incluse entre %s et %s)';

// Error
$_['error_to_name']		= 'Le Nom du destinataire doit être compris entre 1 et 64 caractères !';
$_['error_from_name']	= 'Votre nom doit être compris entre 1 et 64 caractères !';
$_['error_email']		= 'L’Adresse courriel doit être valide !';
$_['error_theme']		= 'Vous devez sélectionner un thème !';
$_['error_amount']		= 'Le Montant doit être compris entre %s et %s !';
$_['error_agree']		= 'Attention, vous devez accepter que les chèques-cadeaux soient non remboursables !';
?>