<?php
//////////////////////////////////////
//									//
// Opencart France					//
// http://www.opencart-france.fr	//
// Traduit par LeorLindel			//
// Exclusivité d’Opencart France 	//
//									//
//////////////////////////////////////

// Heading
$_['heading_title']		= 'Changement de mot de passe';

// Text
$_['text_account']		= 'Compte';
$_['text_password']		= 'Votre mot de passe';
$_['text_success']		= 'Félicitations, votre mot de passe a été mis à jour avec succès !';

// Entry
$_['entry_password']	= 'Mot de passe';
$_['entry_confirm']		= 'Confirmation';

// Error
$_['error_password']	= 'Le Mot de passe doit être compris entre 4 et 20 caractères ! ';
$_['error_confirm']		= 'Le mot de passe et la confirmation ne semble pas correspondre !';
?>