<?php
//
// Opencart catalog module for payment integration with Klik & Pay
//
// Copyright (c) 2010-2018 Cesa Agency pour hosteco.fr
// All rights reserved. ---
//

ini_set('display_errors', 'off');
ini_set('display_startup_errors', 'off');

class ControllerExtensionPaymentKlikandpay extends Controller {

	public function index() {
	  	$this->load->language('extension/payment/klikandpay');
	
	 	$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');

		$data['text_title'] = $this->language->get('text_title');		
		
		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);	
		
    $serveur_klik_pay_T = "https://www.klikandpay.com/paiement/order1.pl";
    $serveur_klik_pay_P = "https://www.klikandpay.com/paiement/order1.pl";

		$serveur = ($this->config->get('payment_klikandpay_test'))? $serveur_klik_pay_T : $serveur_klik_pay_P ;

    $cde_klik = $this->config->get('payment_klikandpay_cde');

    $klikandpay_id = $this->config->get('payment_klikandpay_site');
    $key = $this->config->get('payment_klikandpay_key'); 

// Exemple de g�n�ration de trans_id bas� sur l'horodatage
$ts = time();
$trans_date = date("YmdHis", $ts);
$trans_id = date("His", $ts);

$klikandpay_montant = sprintf("%0.2f",$order_info['total']) ;

//--------------------------------Panier----------------------------------------

    $order_id = $order_info['order_id'];
		$qid_items = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "'");

		$linge_articles = "" ;
    $detail_panier = "";		
    
	if ($qid_items) {
		if ($qid_items->num_rows) {

			foreach ($qid_items->rows as $prod) {			
			$reference = $prod['model']; $quantite = $prod['quantity'];	$prix_ht = $prod['price']+0;
			$nom_produit = $prod['name'];	$tva = $prod['tax'];	$remise_ht = 0;
			
//$linge_articles = $linge_articles.'IDM:'.$site_id.'%REF:'.$reference.'%Q:'.$quantite.'%PRIX:'.sprintf('%0.2f',$prix_ht).'%PROD:'.$nom_produit.'%TVA:'.sprintf('%0.2f',$tva).'%REDUC:'.sprintf('%0.2f',$remise_ht).'|';			
//$linge_articles = utf8_encode(html_entity_decode($linge_articles));
$linge_articles = '';
			}
		}
	}	
/*  	
		$transport = $this->db->query("SELECT value, title FROM `" . DB_PREFIX . "order_total` WHERE sort_order = '3' and order_id = '" . (int)$order_id . "'");  	
	if ($transport) {
		$tr_tax = $this->db->query("SELECT rate FROM `" . DB_PREFIX . "tax_rate` WHERE tax_class_id = '".$this->config->get('tax_class_id')."' ");
		foreach ($tr_tax->rows as $val_tax) { $tr_tva = sprintf("%0.2f",$val_tax['rate']);}
    if(! isset($tr_tva)) {$tr_tva = '0.00';} 

		if ($transport->num_rows) {  	
			foreach ($transport->rows as $lgtr) { 	
  	
//$linge_transport = "IDM:".$site_id."%REF:Transport%Q:1%PRIX:".sprintf("%0.2f",$lgtr['value'])."%PROD:".$lgtr['title']."%TVA:".$tr_tva."%REDUC:0.00|";
//$linge_transport = utf8_encode(html_entity_decode($linge_transport));
$linge_transport = '';
			}
		}
	}	
*/
//$detail_panier = $linge_articles.$linge_transport;

$detail_panier = '';

//------------------------------------------------------------------------------

    $data['action']= $serveur;

		$data['klikandpay_montant'] = $klikandpay_montant;	
		$data['detail_panier'] = $detail_panier;	
		$data['order_id']	= $this->session->data['order_id'];
		$data['order_email']	= $order_info['email'];
		
		if(substr($this->session->data['language'], 0, 2) != 'fr') {$language = 'en';} else {$language = 'fr';}
    $data['langue']	= $language;

		$data['klikandpay_id']	= $klikandpay_id;   
    if(empty($order_info['fax'])) {$order_info_fax = '1213456789';}else{$order_info_fax = $order_info['fax'];} 	
 
    $data['cust_lname']	= $order_info['payment_lastname'];
    $data['cust_fname']	= $order_info['payment_firstname'];
    $data['cust_societe']	= $order_info['payment_company'];
    $data['cust_phone']	= $order_info['telephone'];
    $data['cust_fax']	= $order_info_fax;    
    $data['cust_address']	= $order_info['payment_address_1']." ".$order_info['payment_address_2'];    
    $data['cust_zip']	= $order_info['payment_postcode'];
    $data['cust_city']	= $order_info['payment_city'];
    $data['cust_zone']	= $order_info['payment_zone'];    
    $data['cust_country']	= strtoupper(substr($order_info['payment_country'],0,2));         		

    $data['liv_lname']	= $order_info['shipping_lastname'];
    $data['liv_fname']	= $order_info['shipping_firstname'];
    $data['liv_societe']= $order_info['shipping_company'];
    $data['liv_phone']	= $order_info['telephone'];
    $data['liv_fax']  	= $order_info_fax;
    $data['liv_address']= $order_info['shipping_address_1']." ".$order_info['shipping_address_2'];    
    $data['liv_zip']	  = $order_info['shipping_postcode'];
    $data['liv_city'] 	= $order_info['shipping_city'];
    $data['liv_country']= strtoupper(substr($order_info['shipping_country'],0,2));         
		
		$invoice	= $this->session->data['order_id'] . ' - ' . html_entity_decode($order_info['payment_firstname'], ENT_QUOTES, 'UTF-8') . ' ' . html_entity_decode($order_info['payment_lastname'], ENT_QUOTES, 'UTF-8');
		$lc		= $this->session->data['language'];
			
		$custom = $order_info['order_id'];		
		
    $data['order_id_crypt']	= $order_info['order_id'];

		$data['url_success'] = ''; 
		 
    $data['url_refused']	= '';
     
				return $this->load->view('extension/payment/klikandpay', $data);    		
		
	}

		public function klik_pay_retour() { 
		
    $site_id = $this->config->get('payment_klikandpay_site');
    $key_ret = $this->config->get('payment_klikandpay_key'); 
    
    //echo $key_ret;
    //echo $_GET[$key_ret]; exit;
    //$rqe = print_r($_REQUEST,TRUE);
    $rqe = '';

if (!isset($_REQUEST['NUMXKP'])) $_REQUEST['NUMXKP'] = '';
if($this->config->get('payment_klikandpay_test') == '1'){
$_REQUEST['NUMXKP'] = 'TEST';}
$NUMXKP = $_REQUEST['NUMXKP'];

if (!isset($_REQUEST['PAIEMENT'])) $_REQUEST['PAIEMENT'] = 'nc';
$PAIEMENT = $_REQUEST['PAIEMENT'];

if (!isset($_REQUEST['MONTANTXKP'])) $_REQUEST['MONTANTXKP'] = 'nc';
$MONTANTXKP = $_REQUEST['MONTANTXKP']; // * doit �tre coche dans le back office
if (!isset($_REQUEST['DEVISEXKP'])) $_REQUEST['DEVISEXKP'] = 'nc';
$DEVISEXKP = $_REQUEST['DEVISEXKP']; // * doit �tre coche dans le back office
if (!isset($_REQUEST['IPXKP'])) $_REQUEST['IPXKP'] = 'nc';
$IPXKP = $_REQUEST['IPXKP']; // * doit �tre coche dans le back office
if (!isset($_REQUEST['PAYSRXKP'])) $_REQUEST['PAYSRXKP'] = 'nc';
$PAYSRXKP = $_REQUEST['PAYSRXKP']; // * doit �tre coche dans le back office
if (!isset($_REQUEST['SCOREXKP'])) $_REQUEST['SCOREXKP'] = 'nc';
$SCOREXKP = $_REQUEST['SCOREXKP']; // * doit �tre coche dans le back office
if (!isset($_REQUEST['PAYSBXKP'])) $_REQUEST['PAYSBXKP'] = 'nc';
$PAYSBXKP = $_REQUEST['PAYSBXKP']; // * doit �tre coche dans le back office

if(isset($_REQUEST['RESPONSE'])){$RESPONSE = $_REQUEST['RESPONSE'];}
else {$RESPONSE = '';}
//if($this->config->get('payment_klikandpay_test') == '1'){$RESPONSE = '00'; }
$mess_test = "\n\n- ".$NUMXKP."\n- ".$PAIEMENT."\n- ".$MONTANTXKP."\n- ".$DEVISEXKP."\n- ".$IPXKP."\n- ".$PAYSRXKP."\n- ".$SCOREXKP."\n- ".$PAYSBXKP."\n- ".$RESPONSE."\n- ".$_SERVER["REMOTE_ADDR"]."\n- ".$rqe;

/*
NUMXKP 	 Oui 	Num�ro de transaction Klik & Pay
PAIEMENT Oui 	Type de paiement (1)

MONTANTXKP 	Montant de la transaction
DEVISEXKP 	Devise de la transaction
IPXKP 		  IP du client
PAYSRXKP 		Pays du client par rapport � son IP
SCOREXKP 		Scoring de la transaction (2)
PAYSBXKP 		Pays de la banque de la carte (3)

*/

//---------------------------- Gestion de la Commande et envoie des emails --------------------------------------------------------

		if (isset($_GET[$key_ret])) { //echo $cde_klik;
			$order_id = $_GET[$key_ret];        
		} else {
			$order_id = 0;
		}

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);
		
   	$language = $this->model_localisation_language->getLanguage($order_info['language_id']);	

    $langue = substr($language['code'], 0, 2);

if($this->config->get('payment_klikandpay_test') == '1'){
$site = $this->config->get('config_name')." - ".$_SERVER["HTTP_HOST"]; 
$email = $this->config->get('config_email');

mail("TEST Boutique $site<klikandpay@host-eco.fr>","test (Request) Retour KilkandPay sur $site","$mess_test","From:Boutique $site<$email>");
if($langue == 'fr') { $test = utf8_encode("<b> en Mode Test<br />Cette transaction n'abouti � aucune livraison</b>"); }
if($langue != 'fr') { $test = '<b> in Test Mode<br />This transaction lead to no delivery</b>'; }
} else { $test = ''; }
		
	if ($order_info) {

if(($NUMXKP !="")&&($RESPONSE == '00')) {	
      			
			// Payment has been accepted on the productive server

      if($langue == 'fr'){$comment = 'Paiement Accept&eacute par Klik and Pay'.$test;} else {$comment = 'Accepted Payment by Klik and Pay'.$test;}
   		$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_klikandpay_order_status_id'), $comment, true);

			} elseif($RESPONSE != '00') {

			// Payment has been refused
    $order_status_id = '8'; 
	  if($langue == 'fr'){$commentR = 'Refus&eacute;e par Klik and Pay'.$test;} else {$commentR = 'Refused by Klik and Pay'.$test;} 
		$order_query = $this->db->query("SELECT *, l.filename AS filename, l.directory AS directory FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "language l ON (o.language_id = l.language_id) WHERE o.order_id = '" . (int)$order_id . "' ");
		 
		if ($order_query->num_rows) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "' WHERE order_id = '" . (int)$order_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '1', comment = '" . $this->db->escape($commentR) . "', date_added = NOW()");
      }		
			}
		 
		}
	}
}
// Copyright (c) 2010-2018 Cesa Agency pour hosteco.fr
// All rights reserved. ---
//
?>
