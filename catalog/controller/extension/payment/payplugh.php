<?php
// Copyright (c) 2013-2018 Cee Agency pour hosteco.fr
// All rights reserved. ---

class ControllerExtensionPaymentPayplugh extends Controller {

	public function index() {
//		$this->load->language('extension/payment/payplugh');	
    	
	 	$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_back'] = $this->language->get('button_back');

		$this->load->model('checkout/order');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);	

    $amounts = sprintf("%0.0f",$order_info['total'] * 100 ) ;
    $order_plug = $this->session->data['order_id']."-".strtolower(substr($this->session->data['language'], 0, 2));  //$order_info['order_id'];

//------------------------------------------------------------------------------

       $jsonAnswer = json_decode(html_entity_decode($this->config->get('payment_payplugh_cfg_site')));

       $url_payment = $jsonAnswer->url ;
       $privatekey = $jsonAnswer->yourPrivateKey ;
       $baseReturnUrl = HTTPS_SERVER . 'index.php?route='; 
       $site = $this->config->get('config_name');
       
       $params = array(
                        'amount'=>$amounts,
                        'custom_datas'=>'nous vous remercions de votre achat sur '.$site,
                        'currency'=>'EUR',
                        'ipn_url'=>$baseReturnUrl.'extension/payment/payplugh/payplug_ret_serv',
                        'cancel_url'=>$baseReturnUrl.'checkout/checkout',
                        'return_url'=>$baseReturnUrl.'checkout/success',
                        'email'=>$order_info['email'],
                        'firstname'=>$order_info['payment_firstname'],
                        'lastname'=>$order_info['payment_lastname'],
                        'order'=>$order_plug,
                        'customer'=>$order_info['customer_id']
                        );
                        
        $url_params = http_build_query($params);

        openssl_sign($url_params, $signature, $privatekey, OPENSSL_ALGO_SHA1);
        $url_param_base_encode = base64_encode($url_params);
        $signature_64 = base64_encode($signature);
              
        $url_payplug =($url_payment."?data=".urlencode($url_param_base_encode)."&sign=".urlencode($signature_64));
	    	$data['url_payplug'] =  $url_payplug;

//------------------------------------------------------------------------------		
		
				return $this->load->view('extension/payment/payplugh', $data);
}

		public function payplug_ret_serv() {
		$retour = "payplug_ret_serv";
    	
//---------------------------- Signature de Retour -----------------------------

//define getallheaders function for nginx web server
if(!function_exists('getallheaders'))
{
    function getallheaders()
    {
        $headers = array();
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                $headers[$name] = $value;
            } else if ($name == "CONTENT_TYPE") {
                $headers["Content-Type"] = $value;
            } else if ($name == "CONTENT_LENGTH") {
                $headers["Content-Length"] = $value;
            } else{
                $headers[$name]=$value;
            }  
       }
       return $headers;
    }
} 

// Get data from http request
$headers = getallheaders();

// Avoid problems with lowercase/uppercase transaformations
$headers = array_change_key_case($headers, CASE_UPPER); 
if(!isset($headers['PAYPLUG-SIGNATURE'])){
    header($_SERVER['SERVER_PROTOCOL'] . ' 403 Signature not provided', true, 403);
    die;
}
$signature = base64_decode($headers['PAYPLUG-SIGNATURE']);
$body = file_get_contents('php://input');
$data = json_decode($body, true);

    // Check signature
       $jsonAnswer = json_decode(html_entity_decode($this->config->get('payment_payplugh_cfg_site')));
       $publicKey = $jsonAnswer->payplugPublicKey ;
    
    if((isset($body))&&(isset($signature))){
    $checkSignature = openssl_verify($body , $signature, $publicKey, OPENSSL_ALGO_SHA1);
    } else { header("Location: index.php?route=extension/payment/payplugh/".$retour."."); exit; }	
    if ($checkSignature == 1){
        $bool_sign = true;
    } else if ($checkSignature == 0){
        echo 'Invalid signature';
        header($_SERVER['SERVER_PROTOCOL'] . ' 403 Invalid signature', true, 403);
        die;
    } else {
        echo 'Error while checking signature';
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Error while checking signature', true, 500);
        die;
    }
    
    if ($data && $bool_sign){
    

    $statut = $data['status'];
    $order_id = $data['order'];
    $customer = $data['customer'];
    $id_trans = $data['id_transaction'];
    $cde_info = $data['custom_datas'];

    $lg_order = strlen($order_id);
    $langue   = strtolower(substr($order_id,$lg_order-2,2));
    $order_id = substr($order_id,0,$lg_order-3); 

        // If existing order
        if($order_id){

//---------------------------- Gestion de la Commande et envoie des emails --------------------------------------------------------

		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);

	if ($order_info) { //if($langue != 'en') {$langue = 'fr';}
	
		if($this->config->get('payment_payplugh_test') == '1') {
	if($langue == 'fr') { $test = ' en Mode Test<br />Cette transaction n\'abouti à aucune livraison'; }
	if($langue != 'fr') { $test = ' in Test Mode<br />This transaction lead to no delivery'; }
	} else { $test = ' '; }	
		
       // If status paid
      if ($data['state'] == 'paid') {	
      
      if($langue == 'fr'){$comment = 'Paiement Accepté par payplug'.$test;} else {$comment = 'Accepted Payment by payplug'.$test;}
    	$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_payplugh_order_status_id'), $comment, true);
			$this->db->query("UPDATE `" . DB_PREFIX . "order_history` SET notify = '1', comment = '" . $this->db->escape($comment) . "' WHERE order_status_id = '" .$this->config->get('payment_payplugh_order_status_id'). "' and comment = '' and order_id = '" . (int)$order_id . "' ");
				
      // If status remboursement
			} elseif ($data['state'] == 'refunded') {			
			//	$this->model_checkout_order->refund($order_id);
    $order_status_id = '11'; 
	  if($langue == 'fr'){$commentR = 'Rembours&eacute; par payplug';} else {$commentR = 'Refunded by payplug';} 
		$order_query = $this->db->query("SELECT *, l.filename AS filename, l.directory AS directory FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "language l ON (o.language_id = l.language_id) WHERE o.order_id = '" . (int)$order_id . "' ");
		 
		if ($order_query->num_rows) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "' WHERE order_id = '" . (int)$order_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '1', comment = '" . $this->db->escape($commentR) . "', date_added = NOW()");
      }
      
      // If status refusé      	
			} elseif ($data['state'] == 'fails') {
			//	$this->model_checkout_order->refus($order_id);
    $order_status_id = '8'; 
	  if($langue == 'fr'){$commentR = 'Refus&eacute;e par payplug';} else {$commentR = 'Refused by payplug';} 
		$order_query = $this->db->query("SELECT *, l.filename AS filename, l.directory AS directory FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "language l ON (o.language_id = l.language_id) WHERE o.order_id = '" . (int)$order_id . "' ");
		 
		if ($order_query->num_rows) {
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "' WHERE order_id = '" . (int)$order_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . $order_status_id . "', notify = '1', comment = '" . $this->db->escape($commentR) . "', date_added = NOW()");
		      }
      	
			}			
    }
    
  }      
        // Else invalid order
        else {
        echo 'Error : Order Id invalid.';    
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Missing or wrong parameters', true, 400);
        exit;
        }
        
    } else {
        echo 'Error : missing or wrong parameters.';
        header($_SERVER['SERVER_PROTOCOL'] . ' 400 Missing or wrong parameters', true, 400);
        exit;
    }			
	  
 }
 
}
// Copyright (c) 2013-2018 Cee Agency pour hosteco.fr
// All rights reserved. ---
?>
