<?php
/**
 * Systempay V2-Payment Module version 4.0.0 for OpenCart 3.x. Support contact : supportvad@lyra-network.com.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author    Lyra Network (http://www.lyra-network.com/)
 * @copyright 2014-2018 Lyra Network and contributors
 * @license   http://www.gnu.org/licenses/gpl.html  GNU General Public License (GPL v3)
 * @category  payment
 * @package   systempay
 */

require_once('systempay.php');

class ControllerExtensionPaymentSystempayMulti extends ControllerExtensionPaymentSystempay
{

    public function __construct($params)
    {
        parent::__construct($params);

        $this->name = 'systempay_multi';
    }

    protected function getSystempayRequest()
    {
        $systempayRequest = parent::getSystempayRequest();

        if ($systempayRequest->get('amount')) {
            $currency = SystempayApi::findCurrencyByNumCode($systempayRequest->get('currency'));
            $amount = $currency->convertAmountToFloat($systempayRequest->get('amount'));

            // multi payment options
            $configFirst = $this->config->get($this->prefix . 'systempay_multi_first');
            $first = $configFirst ?
                $currency->convertAmountToInteger($configFirst / 100 * $amount) :
                null;

            $systempayRequest->setMultiPayment(
                null /* use already set amount */,
                $first,
                $this->config->get($this->prefix . 'systempay_multi_count'),
                $this->config->get($this->prefix . 'systempay_multi_period')
            );
        }

        return $systempayRequest;
    }
}
