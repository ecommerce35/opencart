<?php 

$language_id = 2;
foreach($data['languages'] as $language) {
	if($language['language_id'] != 1) {
		$language_id = $language['language_id'];
	}
}

$output = array();
$output["custom_module_module"] = array (
  1 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="row row-no-padding">
   <div class="col-sm-6">
      <a href="#"><img src="image/catalog/default_new/banner-01.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-6">
      <a href="#"><img src="image/catalog/default_new/banner-02.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>
</div>

<div class="row row-no-padding">
   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-03.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-04.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-05.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>
</div>',
      $language_id => '<div class="row row-no-padding">
   <div class="col-sm-6">
      <a href="#"><img src="image/catalog/default_new/banner-01.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-6">
      <a href="#"><img src="image/catalog/default_new/banner-02.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>
</div>

<div class="row row-no-padding">
   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-03.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-04.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>

   <div class="col-sm-4">
      <a href="#"><img src="image/catalog/default_new/banner-05.jpg" alt="Banner" style="display:block;margin:0px auto"></a>
   </div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'slideshow',
    'status' => '1',
    'sort_order' => '2',
  ),
  2 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="row banners">
 <div class="col-sm-6"><a href="#"><img src="image/catalog/default_new/banner-06.jpg" alt="Image"></a></div>
 <div class="col-sm-6"><a href="#"><img src="image/catalog/default_new/banner-07.jpg" alt="Image"></a></div>
 </div>',
      $language_id => '<div class="row banners">
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-06.png" alt="Image"></a></div>
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-07.png" alt="Image"></a></div>
 </div>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '4',
  ),
  3 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="row banners hidden-xs hidden-sm">
 <div class="col-sm-12"><a href="#"><img src="image/catalog/default_new/banner-08.jpg" alt="Image"></a></div>
 </div>',
      $language_id => '<div class="row banners hidden-xs hidden-sm">
 <div class="col-sm-12"><a href="#"><img src="image/catalog/default_new/banner-08.jpg" alt="Image"></a></div>
 </div>',
    ),
    'layout_id' => '1',
    'position' => 'column_left',
    'status' => '1',
    'sort_order' => '2',
  ),
  4 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="row banners hidden-xs hidden-sm">
 <div class="col-sm-12"><a href="#"><img src="image/catalog/banner-09.png" alt="Image"></a></div>
 </div>',
      $language_id => '<div class="row banners hidden-xs hidden-sm">
 <div class="col-sm-12"><a href="#"><img src="image/catalog/banner-09.png" alt="Image"></a></div>
 </div>',
    ),
    'layout_id' => '3',
    'position' => 'column_left',
    'status' => '1',
    'sort_order' => '4',
  ),
  5 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '<p><br></p>',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="row banners">
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-06.png" alt="Image"></a></div>
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-07.png" alt="Image"></a></div>
 </div>',
      $language_id => '<div class="row banners">
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-06.png" alt="Image"></a></div>
 <div class="col-sm-6"><a href="#"><img src="image/catalog/banner-07.png" alt="Image"></a></div>
 </div>',
    ),
    'layout_id' => '2',
    'position' => 'content_bottom',
    'status' => '1',
    'sort_order' => '0',
  ),
  6 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="lookbook">
	<div class="row">
		<div class="col-sm-4">
			<div class="info">
				<div class="first-heading">The new Lookbook</div>
				<div class="second-heading">Just pin everything you love!</div>
				<link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">
				<a href="#" class="button">Read more</a>
			</div>
		</div>
		
		<div class="col-sm-8">
			<div class="image">
				<img src="image/catalog/default_new/lookbook.jpg" alt="">
				<div class="pin" style="left: 67%;top: 72.82%"><a href="#" data-toggle="tooltip" data-original-title="Wrangler jeans - $189.99">1</a></div>
				<div class="pin" style="left: 4%;top: 36.5%"><a href="#" data-toggle="tooltip" data-original-title="Sunglasses - $119.99">2</a></div>
				<div class="pin" style="left: 62.4%;top: 33%"><a href="#" data-toggle="tooltip" data-original-title="Sweater - $289.99">3</a></div>
				<div class="pin" style="left: 27.46%;top: 36.72%"><a href="#" data-toggle="tooltip" data-original-title="Necklace - $389.99">4</a></div>
			</div>
		</div>
	</div>
</div>',
      $language_id => '<div class="lookbook">
	<div class="row">
		<div class="col-sm-4">
			<div class="info">
				<div class="first-heading">The new Lookbook</div>
				<div class="second-heading">Just pin everything you love!</div>
				<link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">
				<a href="#" class="button">Read more</a>
			</div>
		</div>
		
		<div class="col-sm-8">
			<div class="image">
				<img src="image/catalog/default_new/lookbook.jpg" alt="">
				<div class="pin" style="left: 67%;top: 72.82%"><a href="#" data-toggle="tooltip" data-original-title="Wrangler jeans - $189.99">1</a></div>
				<div class="pin" style="left: 4%;top: 36.5%"><a href="#" data-toggle="tooltip" data-original-title="Sunglasses - $119.99">2</a></div>
				<div class="pin" style="left: 62.4%;top: 33%"><a href="#" data-toggle="tooltip" data-original-title="Sweater - $289.99">3</a></div>
				<div class="pin" style="left: 27.46%;top: 36.72%"><a href="#" data-toggle="tooltip" data-original-title="Necklace - $389.99">4</a></div>
			</div>
		</div>
	</div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'preface_fullwidth',
    'status' => '1',
    'sort_order' => '2',
  ),
  7 => 
  array (
    'type' => '2',
    'block_heading' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'block_content' => 
    array (
      1 => '',
      $language_id => '',
    ),
    'html' => 
    array (
      1 => '<div class="new-promotion-columns stationery2-columns">
	<div class="row">
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-free-shipping.png" alt="Free shipping">
				<p>Free Shipping &amp; Returns</p>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-money.png" alt="Money">
				<p>100% Money refund</p>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-delivery.png" alt="Delivery">
				<p>Fast send and delivery</p>
			</div>
		</div>
	</div>
</div>',
      $language_id => '<div class="new-promotion-columns stationery2-columns">
	<div class="row">
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-free-shipping.png" alt="Free shipping">
				<p>Free Shipping &amp; Returns</p>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-money.png" alt="Money">
				<p>100% Money refund</p>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="single-promo-wrapper"> 
				<img src="image/catalog/default_new//icon-delivery.png" alt="Delivery">
				<p>Fast send and delivery</p>
			</div>
		</div>
	</div>
</div>',
    ),
    'layout_id' => '1',
    'position' => 'slideshow',
    'status' => '1',
    'sort_order' => '3',
  ),
); 

$output2 = array();
$output2["custom_module_module"] = $this->config->get('custom_module_module');

if(!is_array($output["custom_module_module"])) $output["custom_module_module"] = array();
if(!is_array($output2["custom_module_module"])) $output2["custom_module_module"] = array();
$output3 = array();
$output3["custom_module_module"] = array_merge($output["custom_module_module"], $output2["custom_module_module"]);

$this->model_setting_setting->editSetting( "custom_module", $output3 );	

?>