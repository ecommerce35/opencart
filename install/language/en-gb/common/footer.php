<?php
// Text
$_['text_project']       = 'Project Homepage';
$_['text_documentation'] = 'Documentation';
$_['text_support']       = 'Support Forums';
$_['text_footer']        = 'Copyright © 2016 OpenCart - All rights reserved<br />French configuration done by <a href="http://www.hosteco.fr" target="_blank">hosteco.fr</a> for Opencart-France.eu';
