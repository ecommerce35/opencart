<?php
// Heading
$_['heading_title']   = 'Mise à jour';
$_['heading_success'] = 'Mise à jour terminée !';

// Text
$_['text_upgrade']    = 'Préparez votre mise à niveau';
$_['text_server']     = 'Vérifiez que votre serveur est configuré correctement';
$_['text_steps']      = 'Progression de la mise à niveau';
$_['text_error']      = 'Postez des erreurs de script de mise à niveau dans les forums';
$_['text_clear']      = 'Après la mise à niveau, supprimez les cookies de votre navigateur pour éviter les erreurs de jetons.';
$_['text_admin']      = 'Chargez la page admin et appuyez deux fois sur Ctrl + F5 pour forcer le navigateur à mettre à jour les modifications CSS.';
$_['text_user']       = "Allez à Admin -> Utilisateurs -> Groupes d'utilisateurs et modifiez le groupe Administrateur supérieur. Cochez toutes les cases.";
$_['text_setting']    = "Allez à Admin et modifiez les principaux paramètres du système. Mettez à jour tous les champs et cliquez sur Enregistrer, même si rien n'a changé.";
$_['text_store']      = 'Chargez le magasin et appuyez deux fois sur Ctrl + F5 pour forcer le navigateur à mettre à jour les modifications CSS.';
$_['text_progress']   = 'Le correctif %s a été appliqué (% s sur% s)';
$_['text_success']    = 'Toutes nos félicitations! Vous avez mis à niveau votre installation OpenCart avec succès !';

// Entry
$_['entry_progress']  = 'Progression';
