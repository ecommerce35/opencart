<?php
// Heading
$_['heading_title']               = 'Installation complète';

// Text
$_['text_step_4']                 = 'Prêt pour commencer à vendre !';
$_['text_catalog']                = 'Aller sur la boutique en ligne';
$_['text_admin']                  = 'Connexion à l’administration';
$_['text_loading']                = 'Chargement des modules...';
$_['text_extension']              = 'Visiter la boutique des extensions';
$_['text_mail']                   = 'Joignez-vous à la liste de diffusion';
$_['text_mail_description']       = 'Restez informé des mises à jour et événements d’OpenCart France.';
$_['text_web']   		              = 'Site Web<br />OpenCart France &trade;';
$_['text_web_description']        = 'Obtenez plus d’information';
$_['text_web_visit']              = 'Visiter notre site OpenCart France &trade;';
$_['text_forum']    	 	          = 'Forums communautaire';
$_['text_forum_description']      = 'Obtenez de l’aide d’autres utilisateurs d’OpenCart France &trade;';
$_['text_forum_visit']            = 'Visiter nos forums';
$_['text_commercial']             = 'Support commercial';
$_['text_commercial_description'] = 'Services de développement des partenaires d’OpenCart France &trade;';
$_['text_commercial_visit']       = 'Visiter la page de nos partenaires';
$_['text_price']   	              = 'Prix';
$_['text_view']                   = 'Voir les détails';
$_['text_download']               = 'Téléchargement';
$_['text_downloads']              = 'Téléchargements';

// Button
$_['button_mail']                 = 'Nous rejoindre ici';
$_['button_setup']                = 'Mise en place maintenant';

// Error
$_['error_warning']               = 'Ne pas oublier de supprimer le répertoire d’installation !';
