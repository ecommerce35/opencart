<?php
// Heading
$_['heading_title']          = 'Pré-Installation';

// Text
$_['text_step_2']            = 'Vérifiez si votre serveur est correctement configuré';
$_['text_install_php']       = '1. Veuillez configurer vos paramètres de PHP afin de correspondre aux exigences énumérées ci-dessous.';
$_['text_install_extension'] = '2. Veuillez vérifier que les extensions PHP ci-dessous sont installés.';
$_['text_install_db']        = '3. Veuillez vous assurer d’avoir au moins un pilote de base de données disponible.';
$_['text_install_file']      = '4. Veuillez vous assurer que vous avez défini les autorisations correctes sur la liste des fichiers ci-dessous.';
$_['text_install_directory'] = '5. Veuillez vous assurer que vous avez défini les autorisations correctes sur la liste des répertoires ci-dessous.';
$_['text_setting']           = 'Paramètres PHP';
$_['text_current']           = 'Paramètres usuel';
$_['text_required']          = 'Paramètres requis';
$_['text_extension']         = 'Paramètres d’extension';
$_['text_db']            	 = 'Base de données';
$_['text_db_driver']         = 'Piote de la base de données';
$_['text_file']              = 'Fichiers';
$_['text_directory']         = 'Répertoires';
$_['text_status']            = 'État';
$_['text_version']           = 'Version PHP';
$_['text_global']            = 'Register Globals';
$_['text_magic']             = 'Magic Quotes GPC';
$_['text_file_upload']       = 'File Uploads';
$_['text_session']           = 'Session Auto Start';
$_['text_gd']                = 'GD';
$_['text_curl']              = 'cURL';
$_['text_mcrypt']            = 'mCrypt';
$_['text_zlib']              = 'ZLIB';
$_['text_zip']               = 'ZIP';
$_['text_mbstring']          = 'mbstring';
$_['text_on']                = 'Activé';
$_['text_off']               = 'Désactivé';
$_['text_writable']          = 'Inscriptible';
$_['text_unwritable']        = 'Non-inscriptible';
$_['text_missing']           = 'Missing';

// Error
$_['error_version']          = 'Warning: You need to use PHP 5.4 or above for OpenCart to work!';
$_['error_file_upload']      = 'Warning: file_uploads needs to be enabled!';
$_['error_session']          = 'Warning: OpenCart will not work with session.auto_start enabled!';
$_['error_db']               = 'Warning: A database extension needs to be loaded in the php.ini for OpenCart to work!';
$_['error_gd']               = 'Warning: GD extension needs to be loaded for OpenCart to work!';
$_['error_curl']             = 'Warning: CURL extension needs to be loaded for OpenCart to work!';
$_['error_mcrypt']           = 'Warning: mCrypt extension needs to be loaded for OpenCart to work!';
$_['error_zlib']             = 'Warning: ZLIB extension needs to be loaded for OpenCart to work!';
$_['error_zip']              = 'Warning: ZIP extension needs to be loaded for OpenCart to work!';
$_['error_mbstring']         = 'Warning: mbstring extension needs to be loaded for OpenCart to work!';
$_['error_catalog_exist']    = 'Warning: config.php does not exist. You need to rename config-dist.php to config.php!';
$_['error_catalog_writable'] = 'Warning: config.php needs to be writable for OpenCart to be installed!';
$_['error_admin_exist']      = 'Warning: admin/config.php does not exist. You need to rename admin/config-dist.php to admin/config.php!';
$_['error_admin_writable']   = 'Warning: admin/config.php needs to be writable for OpenCart to be installed!';
$_['error_image']            = 'Warning: Image directory needs to be writable for OpenCart to work!';
$_['error_image_cache']      = 'Warning: Image cache directory needs to be writable for OpenCart to work!';
$_['error_image_catalog']    = 'Warning: Image catalog directory needs to be writable for OpenCart to work!';
$_['error_cache']            = 'Warning: Cache directory needs to be writable for OpenCart to work!';
$_['error_log']              = 'Warning: Logs directory needs to be writable for OpenCart to work!';
$_['error_download']         = 'Warning: Download directory needs to be writable for OpenCart to work!';
$_['error_upload']           = 'Warning: Upload directory needs to be writable for OpenCart to work!';
$_['error_modification']     = 'Warning: Modification directory needs to be writable for OpenCart to work!';