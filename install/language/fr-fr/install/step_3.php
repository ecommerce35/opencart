<?php
// Heading
$_['heading_title']          = 'Configuration';

// Text
$_['text_step_3']            = 'Entrer la base de données ainsi que les détails de l’administration';
$_['text_db_connection']     = '1. Entrer les détails de la connexion à l’admnistration.';
$_['text_db_administration'] = '2. Entrer le nom d’utilisateur et le mot de passe pour l’administration.';
$_['text_mysqli']            = 'MySQLi';
$_['text_mysql']             = 'MySQL';
$_['text_mpdo']              = 'mPDO';

// Entry
$_['entry_db_driver']        = 'Pilote de BDD';
$_['entry_db_hostname']      = 'Nom d’hôte';
$_['entry_db_username']      = 'Nom d’utilisateur';
$_['entry_db_password']      = 'Mot de passe';
$_['entry_db_database']      = 'Base de données';
$_['entry_db_port']          = 'Port';
$_['entry_db_prefix']        = 'Préfixe';
$_['entry_username']         = 'Nom d’utilisateur';
$_['entry_password']         = 'Mot de passe';
$_['entry_email']            = 'Courriel';

// Error
$_['error_db_hostname'] 	 = 'Nom d’hôte nécessaire !';
$_['error_db_username'] 	 = 'Nom d’utilisateur requis !';
$_['error_db_database']		 = 'Le nom de la base de données est requis !';
$_['error_db_port']		     = 'Le port de la base de données est requis !';
$_['error_db_prefix'] 		 = 'Le préfixe des tables de la base de données ne peut contenir que des caractères minuscules dans la gamme a-z, 0-9 et soulignement';
$_['error_db_connect'] 		 = 'Erreur : Impossible de se connecter à la base de données, veuillez vous assurez-vous que le serveur de base de données, nom d’utilisateur et le mot de passe soit correct !';
$_['error_username'] 		 = 'Nom d’utilisateur requis !';
$_['error_password'] 		 = 'Mot de passe requis !';
$_['error_email'] 			 = 'Courriel invalide !';
$_['error_config'] 			 = 'Erreur : Impossible d’écrire dans config.php, veuillez vérifier que vous avez défini les autorisations correctes sur : ';