<?php
$_['text_license']       = 'Licence';
$_['text_installation']  = 'Pré-Installation';
$_['text_configuration'] = 'Configuration';
$_['text_upgrade']       = 'Mise à jour';
$_['text_finished']      = 'Terminé';
$_['text_language']      = 'Langue';